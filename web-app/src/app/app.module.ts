import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { StateClass } from '@ngxs/store/internals';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { EmployeesState } from './shared/state/employees/employees.state';
import { RouteHandler } from './shared/handler/route.handler';
import { AlertHandler } from './shared/handler/alert.handler';
import { UIState } from './shared/state/ui/ui.state';

const persistentStates: StateClass<any>[] = [EmployeesState];
const states: StateClass<any>[] = [...persistentStates, UIState];

const initFn = () => () => { /* use for some initialization stuff */ };

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxsModule.forRoot(states, { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({ storage: StorageOption.LocalStorage, key: persistentStates }),
    NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production }),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initFn,
      deps: [RouteHandler, AlertHandler],
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
