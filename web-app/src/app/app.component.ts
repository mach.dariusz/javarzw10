import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { interval, Observable } from 'rxjs';
import { IAlert } from './shared/model';
import { SetErrorAlert, SetSuccessAlert } from './shared/state/ui/ui.actions';
import { UIState } from './shared/state/ui/ui.state';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @Select(UIState.getSuccessAlert) successAlert$: Observable<IAlert>;
  @Select(UIState.getErrorAlert) errorAlert$: Observable<IAlert>;

  constructor(private _store: Store) { }

  hideSuccessAlert(): void {
    this._store.dispatch(new SetSuccessAlert({ successAlert: { show: false } }));
  }

  hideErrorAlert(): void {
    this._store.dispatch(new SetErrorAlert({ errorAlert: { show: false } }));
  }
}
