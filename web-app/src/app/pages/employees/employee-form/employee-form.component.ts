import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageModel, PAGE_MODE } from '../../../shared/model';
import { getPageModel, toCamelCase } from '../../../shared/utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { EmployeesState } from '../../../shared/state/employees/employees.state';
import { CreateEmployee, DeleteEmployee, UpdateEmployee } from '../../../shared/state/employees/employees.actions';
import { Location } from '@angular/common';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  pageModel: PageModel;
  employeeForm: FormGroup;

  showErrors = false;

  constructor(
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _store: Store,
    private _location: Location,
  ) { }

  ngOnInit(): void {
    this.pageModel = getPageModel(this._route);
    this.initForm();
  }

  private initForm(): void {

    this.employeeForm = this._fb.group({
      id: [],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      dateOfBirth: [],
      phoneNumber: [],
      email: [],
      status: [],
      company: [],

      // address: this._fb.group({
      //   street: [''],
      //   city: [''],
      //   state: [''],
      //   zip: ['']
      // }),
    });

    if (this.pageModel.pageMode !== PAGE_MODE.CREATE) {
      const employee = this._store.selectSnapshot(EmployeesState.getEmployeeById)(this.pageModel.id);
      this.employeeForm.patchValue(employee);
    }

    if (this.pageModel.pageMode === PAGE_MODE.DETAILS) {
      this.employeeForm.disable();
    }

  }

  getPageModeCamelCase(): string {
    return toCamelCase(this.pageModel.pageMode);
  }

  save(): void {
    this.showErrors = true;

    if (!this.employeeForm.invalid) {
      console.log('ready to save', this.employeeForm.value);

      this._store.dispatch(new CreateEmployee({ employee: this.employeeForm.value })).subscribe(
        success => {
          console.log('request success', success);
        },
        error => {
          console.error('request errored:', error);
        },
        () => { console.log('request completed'); },
      );
    } else {
      console.log('not ready to save', this.employeeForm.value);
    }
  }

  update(): void {
    this._store.dispatch(new UpdateEmployee({ employee: this.employeeForm.value }));
  }

  delete(): void {
    this._store.dispatch(new DeleteEmployee({ id: this.employeeForm.value.id }));
  }

  goBack(): void {
    this._location.back();
  }

  ifRequired(value: string): boolean {
    const field = this.employeeForm.get(value);

    return this.showErrors ? field.errors?.required : field.touched && field.errors?.required;
  }
}
