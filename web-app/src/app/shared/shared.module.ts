import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { TextInputComponent } from './component/text-input/text-input.component';

@NgModule({
  declarations: [TextInputComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ],
  exports: [
    FontAwesomeModule,
    ReactiveFormsModule,
    TextInputComponent,
  ]
})
export class SharedModule { }
