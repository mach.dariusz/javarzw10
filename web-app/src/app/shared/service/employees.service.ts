import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Employee, EmployeePaginationAndSortResponse } from '../model';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  private host = environment.host;
  private employeeService = `${this.host}/api/v1/employees-service`;
  private baseEmployeesUrl = `${this.employeeService}/employees`;

  constructor(private _http: HttpClient) { }

  getAllEmployees(): Observable<EmployeePaginationAndSortResponse> {
    return this._http.get<EmployeePaginationAndSortResponse>(this.baseEmployeesUrl);
  }

  createEmployee(employee: Employee): Observable<Employee> {
    return this._http.post<Employee>(this.baseEmployeesUrl, employee);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this._http.put<Employee>(`${this.baseEmployeesUrl}/${employee.id}`, employee);
  }

  deleteEmployee(id: string): Observable<Employee> {
    return this._http.delete<Employee>(`${this.baseEmployeesUrl}/${id}`);
  }

}
