import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Actions, ofActionSuccessful } from '@ngxs/store';
import { CreateEmployee, DeleteEmployee, UpdateEmployee } from '../state/employees/employees.actions';

@Injectable({
  providedIn: 'root'
})
export class RouteHandler {

  constructor(
    private _actions$: Actions,
    private _location: Location,
  ) {

    // go back if employee created
    this._actions$.pipe(ofActionSuccessful(CreateEmployee, UpdateEmployee, DeleteEmployee)).subscribe(result => {
      this._location.back();
    });
  }
}
