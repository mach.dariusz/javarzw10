import { Injectable } from '@angular/core';
import { Actions, ofActionDispatched, ofActionErrored, ofActionSuccessful, Store } from '@ngxs/store';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { CreateEmployee } from '../state/employees/employees.actions';
import { SetErrorAlert, SetSuccessAlert } from '../state/ui/ui.actions';
import { UIState } from '../state/ui/ui.state';

@Injectable({
  providedIn: 'root'
})
export class AlertHandler {

  constructor(
    private _actions$: Actions,
    private _store: Store,
  ) {
    this._actions$
      .pipe(ofActionSuccessful(CreateEmployee))
      .subscribe(result => {
        this._store.dispatch(new SetSuccessAlert({
          successAlert: {
            show: true, text: 'Employee created!', lenght: 5 * 1000
          }
        }));
      });

    this._actions$
      .pipe(ofActionErrored(CreateEmployee))
      .subscribe(result => {
        this._store.dispatch(new SetErrorAlert({
          errorAlert: {
            show: true, text: 'Employee not created! Error occured!', lenght: 5 * 1000
          }
        }));
      });

    this._actions$
      .pipe(ofActionDispatched(SetSuccessAlert))
      .subscribe((alertChange: SetSuccessAlert) => {
        if (alertChange.payload.successAlert.show) {
          interval(alertChange.payload.successAlert.lenght).pipe(take(1)).subscribe(() => {
            const currentSuccessAlert = this._store.selectSnapshot(UIState.getSuccessAlert);
            if (currentSuccessAlert.show) {
              this._store.dispatch(new SetSuccessAlert({ successAlert: { show: false } }));
            }
          });
        }
      });

    this._actions$
      .pipe(ofActionDispatched(SetErrorAlert))
      .subscribe((alertChange: SetErrorAlert) => {
        if (alertChange.payload.errorAlert.show) {
          interval(alertChange.payload.errorAlert.lenght).pipe(take(1)).subscribe(() => {
            const currentErrorAlert = this._store.selectSnapshot(UIState.getErrorAlert);
            if (currentErrorAlert.show) {
              this._store.dispatch(new SetErrorAlert({ errorAlert: { show: false } }));
            }
          });
        }
      });
  }
}
