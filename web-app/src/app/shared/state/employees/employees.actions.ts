import { Employee } from '../../model';

export class FetchEmployees {
  public static readonly type = '[Employees] Fetch all employees';
}

export class FetchEmployeeById {
  public static readonly type = '[Employees] Fetch employee by id';
  constructor(public payload: { id: string }) { }
}

export class CreateEmployee {
  public static readonly type = '[Employees] Create employee';
  constructor(public payload: { employee: Employee }) { }
}

export class UpdateEmployee {
  public static readonly type = '[Employees] Update employee';
  constructor(public payload: { employee: Employee }) { }
}

export class DeleteEmployee {
  public static readonly type = '[Employees] Delete employee';
  constructor(public payload: { id: string }) { }
}
