import { State, Action, Selector, StateContext } from '@ngxs/store';
import { insertItem, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { CreateEmployee, DeleteEmployee, FetchEmployees, UpdateEmployee } from './employees.actions';
import { Employee } from '../../model';
import { Injectable } from '@angular/core';
import { EmployeesService } from '../../service/employees.service';
import { tap } from 'rxjs/operators';

export interface EmployeesStateModel {
  employees: Employee[];
  recentlyAddedEmployees: Employee[];
  recentlyDeletedEmployees: Employee[];
}

@State<EmployeesStateModel>({
  name: 'employees',
  defaults: {
    employees: [],
    recentlyAddedEmployees: [],
    recentlyDeletedEmployees: [],
  }
})
@Injectable()
export class EmployeesState {

  @Selector()
  public static getEmployees(state: EmployeesStateModel): Employee[] {
    return state.employees;
  }

  @Selector()
  public static getEmployeeById(state: EmployeesStateModel): (id: string) => Employee {
    return (id: string): Employee => {
      return state.employees.find(employee => employee.id === id);
    };
  }

  @Selector()
  public static getRecentlyAddedEmployees(state: EmployeesStateModel): Employee[] {
    return state.recentlyAddedEmployees;
  }

  @Selector()
  public static getRecentlyDeletedEmployees(state: EmployeesStateModel): Employee[] {
    return state.recentlyAddedEmployees;
  }

  constructor(private _employeeService: EmployeesService) { }

  @Action(FetchEmployees)
  public fetchEmployees(ctx: StateContext<EmployeesStateModel>): any {

    return this._employeeService.getAllEmployees().pipe(
      tap(response => {
        ctx.patchState({ employees: response.content });
      }),
    );
  }

  @Action(CreateEmployee)
  public createEmployee(ctx: StateContext<EmployeesStateModel>, { payload }: CreateEmployee): any {

    return this._employeeService.createEmployee(payload.employee).pipe(
      tap(response => {
        ctx.setState(patch({ employees: insertItem(response) }));
      }),
    );
  }

  @Action(UpdateEmployee)
  public updateEmployee(ctx: StateContext<EmployeesStateModel>, { payload }: UpdateEmployee): any {

    return this._employeeService.updateEmployee(payload.employee).pipe(
      tap(response => {
        ctx.setState(patch({ employees: updateItem(emp => emp.id === response.id, response) }));
      }),
    );
  }

  @Action(DeleteEmployee)
  public DeleteEmployee(ctx: StateContext<EmployeesStateModel>, { payload }: DeleteEmployee): any {

    return this._employeeService.deleteEmployee(payload.id).pipe(
      tap(response => {
        ctx.setState(patch({ employees: removeItem((emp: Employee) => emp.id === response.id) }));
      }),
    );
  }

}
