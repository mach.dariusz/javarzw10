import { State, Action, Selector, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { SetSuccessAlert, SetErrorAlert } from './ui.actions';
import { IAlert } from '../../model';

export interface UIStateModel {
  successAlert: IAlert;
  errorAlert: IAlert;
}

@State<UIStateModel>({
  name: 'ui',
  defaults: {
    successAlert: { show: false },
    errorAlert: { show: false },
  }
})
@Injectable()
export class UIState {

  @Selector()
  public static getSuccessAlert(state: UIStateModel): IAlert {
    return state.successAlert;
  }

  @Selector()
  public static getErrorAlert(state: UIStateModel): IAlert {
    return state.errorAlert;
  }

  @Action(SetSuccessAlert)
  public setSuccessAlert(ctx: StateContext<UIStateModel>, { payload }: SetSuccessAlert): any {
    ctx.patchState({ successAlert: payload.successAlert });
  }

  @Action(SetErrorAlert)
  public setErrorAlert(ctx: StateContext<UIStateModel>, { payload }: SetErrorAlert): any {
    ctx.patchState({ errorAlert: payload.errorAlert });
  }

}
